# Informasi kelompok
Kelompok 12 JCC Laravel
Anggota:
1. Geriano
2. Febiannisa Utami
3. Nasihul Umam
Tema Project : Peminjaman Barang

# Link video demo
https://drive.google.com/file/d/1yYILiXEplE1_J5Q-goAHDLTZUv2ApyhD/view?usp=sharing

# Link deploy di hosting sendiri atau heroku. 
http://kelompok12.herokuapp.com

# ERD
https://drive.google.com/file/d/13xwA_CCMqcI8lgaGGc0bWYBrwlaRPBLg/view?usp=sharing