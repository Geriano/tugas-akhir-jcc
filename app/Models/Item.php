<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'stock',
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'categories',
    ];

    /**
     * @var string[]
     */
    protected $withCount = [
        'borrowed',
        'returned',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function borrowed()
    {
        return $this->hasMany(Borrow::class, 'item_id', 'id')
                    ->where('borrow_approved', true)
                    ->where('return_approved', false);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function returned()
    {
        return $this->hasMany(Borrow::class, 'item_id', 'id')
                    ->where('return_approved', true);
    }
}
