<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Kategori Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.category.index')->with([
            'data' => $data,
            'categories' => Category::get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Kategori Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';
        return view('pages.category.create' ,  ['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Category::create($request->validate([
            'name' => 'required|unique:categories|max:255',
            'description' => 'nullable',
        ]));

        return redirect()->route('category.index')->with('success', 'Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';        

        return view('pages.category.show')->with([
            'data' => $data,
            'category' => $category,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';

        return view('pages.category.edit')->with([
            'data' => $data,
            'category' => $category,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $category->update($request->validate([
            'name' => [
                'required',
                'max:255',
                Rule::unique('categories')->ignore($category->id),
            ],
            'description' => 'nullable',
        ]));

        return redirect()->route('category.index')->with('success', 'Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if ($category->delete()) {
            return redirect()->route('category.index')->with('success', 'Data berhasil dihapus');
        }

        return redirect()->route('category.index')->with('error', 'Data gagal dihapus');
    }
}
