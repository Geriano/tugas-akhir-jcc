<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Peminjaman Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';
        // $data['item'] = DB::table('item')->get();
        return view('pages.peminjaman.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Peminjaman Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';
        return view('pages.peminjaman.create' , compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Peminjaman Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';
        return view('pages.peminjaman.show' ,  compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data['layout'] = 'layouts.web';
        $data['page'] = 'Peminjaman Barang';
        $data['app'] = 'Tugas Akhir JCC Kelompok 12';
        return view('pages.peminjaman.edit' , compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
