<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Item;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
        ]);

        $atk = Category::create([
            'name' => 'atk',
            'description' => 'alat tulis kantor',
        ]);

        $elektronik = Category::create([
            'name' => 'elektronik',
            'description' => 'alat elektronik',
        ]);

        $nota = Item::create([
            'name' => 'nota pembayaran',
            'stock' => 10,
        ]);

        $nota->categories()->sync([$atk->id]);

        $calculator = Item::create([
            'name' => 'kalkulator digital',
            'stock' => 10,
        ]);

        $calculator->categories()->sync([$atk->id, $elektronik->id]);

        $laptop = Item::create([
            'name' => 'laptop',
            'stock' => 2,
        ]);

        $laptop->categories()->sync([$elektronik->id]);
    }
}
