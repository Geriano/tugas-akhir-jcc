@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <form action="{{ route('user.update', $user->id) }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama User</label>
      <input type="text" class="form-control" name="name" value="{{ $user->name }}">
    </div>

    @error('name')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Role</label>
      <select name="role" class="custom-select col-12">
        <option @if ($user->role == 'admin') selected @endif value="admin">Admin</option>
        <option @if ($user->role == 'user') selected @endif value="user">Peminjam</option>
      </select>
    </div>

    @error('role')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Umur</label>
      <input type="number" class="form-control" name="age">
    </div>

    @error('age')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection