@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <div class="min-height-200px">
    <div class="page-header">
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="title">
            <h4>Detail User</h4>
          </div>
          <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Detail User</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
    <div class="blog-wrap">
      <div class="container pd-0">
        <div class="row">
          <div class="col-md-8 col-sm-12">
            <div class="blog-detail card-box overflow-hidden mb-30">
              <div class="blog-img">
                <img src="{{asset('deskapp2-master/vendors/images/img2.jpg')}}" alt="">
              </div>
              <div class="blog-caption">
                <h4 class="mb-10">Nama</h4>
                <p>{{ $user->name }}</p>
                <h4 class="mb-10">Umur</h4>
                <p>{{ $user->profile->age ?? 'Belum di tentukan' }}</p>
                <h5 class="mb-10">Email</h5>
                <p>{{ $user->email }}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection