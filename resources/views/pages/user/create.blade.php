@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <form action="{{ route('user.store') }}" method="POST">
    @csrf
    
    <div class="form-group">
      <label>Nama</label>
      <input type="text" class="form-control" name="name">
    </div>

    @error('name')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Email</label>
      <input type="email" class="form-control" name="email">
    </div>

    @error('email')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label>Kategori User</label>
      <select class="custom-select col-12">
        <option selected="">Choose...</option>
        <option value="admin">Admin</option>
        <option value="user">Peminjam</option>
      </select>
    </div>

    @error('category-user')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" class="form-control" name="password">
    </div>

    @error('password')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
      <label for="exampleInputPassword1">Password Konfirmasi</label>
      <input type="password" class="form-control" name="password_confirmation">
    </div>

    @error('password_confirmation')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection