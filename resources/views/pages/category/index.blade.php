@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <div class="card-box mb-30">
    <div class="pd-20">
      <h4 class="text-blue h4">Data Kategori Barang</h4>
      @if(request()->user()->role == 'admin')
      <a href="/category/create" class="btn btn-primary">Tambah</a>          
      @endif
    </div>

    <div class="pb-20">
      <table class="data-table table stripe hover nowrap">
        <thead>
          <tr>
            <th class="table-plus datatable-nosort">Kategori Barang</th>
            <th>Deskripsi Barang</th>                                           
            <th class="datatable-nosort">Action</th>
          </tr>
        </thead>

        <tbody>
          @foreach($categories as $category)
          <tr>
            <td class="table-plus">{{ mb_strtoupper($category->name) }}</td>
            <td>{{ mb_strtoupper($category->description) }}</td>                    
            <td>
              <div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                  <i class="dw dw-more"></i>
                </a>
                
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                  <a class="dropdown-item" href="{{ route('category.show', $category->id) }}"><i class="dw dw-eye"></i> View</a>
                  @if(request()->user()->role == 'admin')
                  <a class="dropdown-item" href="{{ route('category.edit', $category->id) }}"><i class="dw dw-edit2"></i> Edit</a>
                  <form action="{{ route('category.destroy', $category->id) }}" method="post" onsubmit="event.preventDefault(); Swal.fire({
                    text: 'apakah anda yakin?',
                    icon: 'question',
                    showCancelButton: true,
                  }).then(response => response.isConfirmed && event.target.submit())">
                    @csrf
                    @method('delete')
                    <button class="dropdown-item"><i class="dw dw-delete-3"></i> Delete</button>
                  </form>
                  @endif
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection