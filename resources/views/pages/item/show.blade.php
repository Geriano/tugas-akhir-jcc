@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')

<div class="min-height-200px">
  <div class="page-header">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="title">
          <h4>Detail Barang</h4>
        </div>
        <nav aria-label="breadcrumb" role="navigation">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Detail Barang</li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
  <div class="blog-wrap">
    <div class="container pd-0">
      <div class="row">
        <div class="col-md-8 col-sm-12">
          <div class="blog-detail card-box overflow-hidden mb-30">
            <div class="blog-img">
              <img src="{{asset('deskapp2-master/vendors/images/img2.jpg')}}" alt="">
            </div>

            <div class="blog-caption">
              <h4 class="mb-10">Nama Barang</h4>
              <p>{{ $item->name }}</p>
              <h4 class="mb-10">Kategori Barang</h4>
              <p>
                @foreach ($item->categories as $category)
                <span>{{ $category->name }}</span>
                @endforeach
              </p>
              <h4 class="mb-10">Total Stok</h4>
              <p>{{ $item->stock }}</p>
              <h4 class="mb-10">Stok Tersedia</h4>
              <p>{{ $item->stock - $item->borrowed_count }}</p>
              <h4 class="mb-10">Stok Dipinjam</h4>
              <p>{{ $item->borrowed_count }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection