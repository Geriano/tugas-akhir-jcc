@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <div class="card-box mb-30">
    <div class="pb-20">
      <table class="data-table table stripe hover nowrap">
        <thead>
          <tr>
            <th class="table-plus datatable-nosort">Nama Barang</th>
            <th>Peminjam</th>
            <th>Pinjam Tanggal</th>
            <th class="datatable-nosort">Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($borrows as $borrow)
          <tr>
            <td class="table-plus">{{ mb_strtoupper($borrow->item->name) }}</td>
            <td class="table-plus">{{ mb_strtoupper($borrow->user->name) }}</td>
            <td>{{ $borrow->created_at }}</td>
            <td>
              <div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                  <i class="dw dw-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                  <form action="{{ route('approval.return.approve', $borrow->id) }}" method="POST">
                    @csrf
                    <button class="dropdown-item"><i class="dw dw-eye"></i> Approve</button>
                  </form>
                </div>
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection