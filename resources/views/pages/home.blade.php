@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
	{{--
		<h1>Selamat Datang di Sipbar</h1>
		<h2>Silahkan pilih link di bawah sesuai kepentingan</h2>

		<h3>Saya ingin <a href="/peminjaman"></a> meminjam item</h3>
		<h3>Saya ingin <a href="/pengembalian"> mengembalikan item</a></h3>
	--}}

	<div class="card-box pd-20 height-100-p mb-30">
		<div class="row align-items-center">
			<div class="col-md-4">
				<img src="{{asset('deskapp2-master/vendors/images/banner-img.png')}}" alt="">
			</div>
			<div class="col-md-8">
				<div class="weight-600 font-30 text-blue">Selamat Datang di SiPBar</div>
			</div>
		</div>
	</div>
@endsection