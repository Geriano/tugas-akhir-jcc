@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')

<form action="/item" method="POST">
    @csrf
        <div class="form-group">
            <label>Nama Peminjam Barang</label>
            <input type="text" class="form-control" name="nama-item">                
        </div>
        @error('nama-user')
            <div class="alert alert-danger">{{$message}}</div>                
        @enderror
        <div class="form-group">
            <label>Nama Barang</label>
            <input type="text" class="form-control" name="nama-item">                
        </div>
        @error('nama-item')
            <div class="alert alert-danger">{{$message}}</div>                
        @enderror
        <div class="form-group">
            <label>Kategori Barang</label>
            {{-- <input type="text" class="form-control" name="category-item">                 --}}
            <select class="custom-select col-12">
                <option selected="">Choose...</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>
        @error('category-item')
            <div class="alert alert-danger">{{$message}}</div>                
        @enderror
        <div class="form-group">
            <label>Tanggal Peminjaman Barang</label>
            {{-- <input type="text" class="form-control" name="kondisi-item">                 --}}
            <input class="form-control date-picker" placeholder="Select Date" type="text">
        </div>
        @error('tanggal-mulai')
            <div class="alert alert-danger">{{$message}}</div>                
        @enderror       
        <div class="form-group">
            <label>Estimasi Tanggal Pengembalian Barang</label>
            <input class="form-control date-picker" placeholder="Select Date" type="text">
            {{-- <input type="text" class="form-control" name="status-item">                 --}}
        </div>
        @error('tanggal-kembali')
            <div class="alert alert-danger">{{$message}}</div>                
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection