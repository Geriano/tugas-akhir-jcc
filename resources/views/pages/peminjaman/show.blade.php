@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')

<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="title">
                    <h4>Detail Peminjaman Barang</h4>
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Detail Peminjaman Barang</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="blog-wrap">
        <div class="container pd-0">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="blog-detail card-box overflow-hidden mb-30">
                        <div class="blog-img">
                            <img src="{{asset('deskapp2-master/vendors/images/img2.jpg')}}" alt="">
                        </div>
                        <div class="blog-caption">
                            <h4 class="mb-10">Kategori Barang</h4>
                            <p></p>
                            <h5 class="mb-10">Lorem ipsum dolor sit amet, consectetur.</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <h5 class="mb-10">Unordered List</h5>
                            <ul>
                                <li>Duis aute irure dolor in reprehenderit in voluptate.</li>
                                <li>Sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                                <li>Duis aute irure dolor in reprehenderit in voluptate.</li>
                                <li>Sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            </ul>
                            <h5 class="mb-10">Ordered List</h5>
                            <ol>
                                <li>Duis aute irure dolor in reprehenderit in voluptate.</li>
                                <li>Sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                                <li>Duis aute irure dolor in reprehenderit in voluptate.</li>
                                <li>Sunt in culpa qui officia deserunt mollit anim id est laborum.</li>
                                <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="card-box mb-30">
                        <h5 class="pd-20 h5 mb-0">Categories</h5>
                        <div class="list-group">
                            <a href="#" class="list-group-item d-flex align-items-center justify-content-between">HTML <span class="badge badge-primary badge-pill">7</span></a>
                            <a href="#" class="list-group-item d-flex align-items-center justify-content-between">Css <span class="badge badge-primary badge-pill">10</span></a>
                            <a href="#" class="list-group-item d-flex align-items-center justify-content-between active">Bootstrap <span class="badge badge-primary badge-pill">8</span></a>
                            <a href="#" class="list-group-item d-flex align-items-center justify-content-between">jQuery <span class="badge badge-primary badge-pill">15</span></a>
                            <a href="#" class="list-group-item d-flex align-items-center justify-content-between">Design <span class="badge badge-primary badge-pill">5</span></a>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
</div>

@endsection