@extends($data['layout'])
@section('title', $data['page'] . ' | ' . $data['app'])
@section('title_page', $data['page'])
@section('content')
  <div class="card-box mb-30">
    <div class="pd-20">
      <h4 class="text-blue h4">Peminjaman Barang</h4>      
    </div>

    <div class="pb-20">
      <table class="data-table table stripe hover nowrap">
        <thead>
          <tr>
            <th>Barang</th>
            <th>Peminjam</th>
            <th>Tanggal Peminjaman</th>
            <th>Tanggal Pengembalian</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($borrows as $borrow)
          <tr>
            <td class="table-plus">{{ $borrow->item->name }}</td>
            <td>{{ $borrow->user->name }}</td>
            <td>{{ $borrow->borrowed_at }}</td>
            <td>{{ $borrow->returned_at }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
@endsection