<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthenticatedSessionController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\UserController;
use App\Models\Borrow;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [AuthenticatedSessionController::class, 'index']);

Route::middleware(['auth'])->resource('category', KategoriController::class);
Route::middleware(['auth'])->resource('item', BarangController::class);
Route::middleware(['auth'])->post('/item/{item}/borrow', [BarangController::class, 'borrow'])->name('item.borrow');
Route::middleware(['auth'])->resource('user', UserController::class);

Route::middleware(['auth'])->name('approval.borrow.')->prefix('/approval/borrow')->group(function () {
  Route::get('/', function () {
    return view('pages.approval.borrow')->with([
      'data' => [
        'layout' => 'layouts.web',
        'app' => 'Peminjaman',
        'page' => 'Peminjaman',
      ],
      'borrows' => Borrow::with(['user', 'item'])->where('borrow_approved', false)->get(),
    ]);
  })->name('index');

  Route::post('/{borrow}/approve', function (Borrow $borrow) {
    $borrow->update([
      'borrow_approved' => true,
      'borrowed_at' => now(),
    ]);

    return redirect()->route('approval.borrow.index')->with('success', 'Peminjaman berhasil di approve');
  })->name('approve');
});

Route::middleware(['auth'])->name('approval.return.')->prefix('/approval/return')->group(function () {
  Route::get('/', function () {
    return view('pages.approval.return')->with([
      'data' => [
        'layout' => 'layouts.web',
        'app' => 'Pengembalian',
        'page' => 'Pengembalian',
      ],
      'borrows' => Borrow::with(['user', 'item'])->where('borrow_approved', true)->where('return_approved', false)->get(),
    ]);
  })->name('index');

  Route::post('/{borrow}/approve', function (Borrow $borrow) {
    $borrow->update([
      'return_approved' => true,
      'returned_at' => now(),
    ]);

    return redirect()->route('approval.return.index')->with('success', 'Pengembalian berhasil di approve');
  })->name('approve');
});

Route::get('/peminjaman', fn () => view('pages.peminjaman.index')->with([
  'data' => [
    'layout' => 'layouts.web',
    'app' => 'Pengembalian',
    'page' => 'Pengembalian',
  ],
  'borrows' => Borrow::with(['user', 'item'])->get(),
]));

Route::get('/', [HomeController::class, 'index'])->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
